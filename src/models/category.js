import mongoose from 'mongoose';

const categorySchema = new mongoose.Schema({
    category_name: {
        type: String,
        required: true
    }
});

const Category = mongoose.model('Category', categorySchema, 'category');

export default Category
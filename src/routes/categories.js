import express from 'express';

import CategoryController, { CATEGORY_PREFIX } from "../controllers/category";

const router = express.Router();
const category_controller = new CategoryController();

router
    .get('/', category_controller.getAll)
    .get('/forms', category_controller.getForms)
    .post('/create', category_controller.validate(), category_controller.create)
    .post('/update', category_controller.validate(), category_controller.update)
    .post('/delete', category_controller.delete);

export default router
export { CATEGORY_PREFIX }
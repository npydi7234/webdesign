## Run Airport CRUD operations Server 
>npm start
>node --experimental-modules --es-module-specifier-resolution=node server.js

## Access Airport CRUD operations Server from browser 
> http://localhost:80/Airport

## License

Open sourced under [MIT license](LICENSE.txt).
The MIT License is short and simple to the point. It lets people do almost anything they want with my project, like making and distributing closed source versions.


